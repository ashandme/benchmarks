#include <stdio.h>
unsigned int fibonacci(unsigned int n){
    switch (n) {
        case 0 : return 1;
        case 1 : return 1;
        default : return (fibonacci(n - 1) + fibonacci(n - 2));
    }
}
int main() {
    printf("fibonacci of 45 is %d\n", fibonacci(45));
}
