package main

import "fmt"

func fibonacci(n uint32) uint32 {
    switch n {
        case 0:
            return 1
        case 1:
            return 1
        default:
            return (fibonacci(n - 1) + fibonacci(n - 2))
    }
}
func main() {
    fmt.Println("fibonacci of 45 is", fibonacci(45))
}
