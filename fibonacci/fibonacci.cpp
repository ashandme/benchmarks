#include <iostream>
unsigned int fibonacci(unsigned int n) {
    switch(n){
        case 0: return 1;
        case 1: return 1;
    	default: return(fibonacci(n - 1) + fibonacci(n - 2)); 
    }
}
int main() {
    std::cout << "fibonacci of 45 is " << fibonacci(45) << std::endl;
}
