benchmarks
==========
Just some simple benchmarks.

.. image:: meme.png

Please, checkout the CI/CD section at: https://gitlab.com/renich/benchmarks/-/jobs.
